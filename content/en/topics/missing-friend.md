# My friend is Missing


Objective is to locate the missing person , and in case of detainment and confiscation of personal items, to secure their digital assets.


[introduction] Sometimes it is helpful to secure their digital assets as much and as soon as possible, to reduce their problems while detained. Information about the missing person can be misused in a court of law or by the detainers. Repressive governments and criminal groups are making more use of information received from social media accounts as Facebook and Twitter. The use of security apps or tools is criminalized by more and more governments.  

## workflow

### nature_of_missing

Do you think the person is missing because of his/her activism? 

  - [Yes](#because_activism)
  - [No](#ask_crime)
  
### because_activism

> In some cases blocking the missing person's online accounts can help in reducing the risks he may face with authorities
> What is online about your friend can be seen by the detainers: the work he/she does and personal and professional associations. In most cases you may want to limit the personal details or political affiliations revealed online such as on social media sites.

Do you want to block the missing person's online accounts?

  - [Yes](#contact_an_block)
  - [No](#ask_track)

### ask_crime

Do you think the missing person is object of kidnap or abduction?

  - [Yes](#hostage_uk)
  - [No](#track)
  
### hostage_uk

[#HostageUK](https://hostakeuk.org) is an organisation to ensure that hostages and their families get the support and guidance they need to survive the challenge of a hostage situation. They provide 24/7 support free of charge and on a fully confidential basis. Hostage UK can advise you if it would be necessary to block accounts. 

Always consider that blocking the missing person's online accounts can help in reducing the risks he/she may face.

What is online about the missing person can be seen by the detainers: the work he/she does and personal and professional associations. In most cases you may want to limit the personal details revealed online such as on social media sites.

If you would like to block the missing person's accounts go to [AccessNow's helpline](https://www.accessnow.org/help/)

### ask_track
> If the missing person was carring a phone or other device this can be used to track them down.

Would you like to try to track them down threw their devices?

  - [Yes](#track)
  - [No](#)

### track

> Some smartphones have a function to localize them in case they get lost or stolen, this can be used to track down the position of the device and in some cases the places it was before.

> If you have access to the missing person's iCloud or gmail accounts asociated with their phones, using Apple's or Goole's website you can access this information

> If they have installed and used other traking software please refer to the proper website. We have recommended the use of [Prey](https://preyproject.com/) for this.
    
What type of account is it?

  - [iPhone / iCloud](#iphone)
  - [Android / gmail](#android)
  - [Prey](#Prey)
  - [I do not have the credentials](#contact_an_localizing)

If they have installed and used other traking software please refer to the proper website. We have recommended the use of [Prey](https://preyproject.com/) for this.

### iphone

Find the approximate location of your iOS device, Apple Watch, AirPods, or Mac computer using Find My iPhone on iCloud.com. 

For your an iPhone to be located there are some requierments that should be met:
- Find My iPhone is set up on the device
- The device is online.

go to Apple's [Find My iPhone ](https://www.icloud.com/#find), 

Continue

### android

For your android phone to be located there are some requierments that should be met:
- Your device is connected with your Google account.
- Your device has access to the internet.
- Allowed Find My Device to locate your device (turned on by default). This can changed in the Google Settings app.
- Allowed Find My Device to lock your device and erase its data (turned off by default).

Google implemented some of the features into their search results page. This means that you’re able to quickly locate any registered Android device right from the search results. By using the search phrase “where is my phone”, if you have their gmail account open, Google displays a little map above the search results in which it will try to find your lost Android phone. Once found, you can let it ring by clicking on “Ring”.

[Find My Device](https://www.google.com/android/find) is Google’s official and easy-to-use tool to track your lost Android phone or tablet.

Continue

### prey

Prey is a tool used to control devices over distance.  One of its functions is to keep track of the the devices GPS positioning. Prey will work with Windows, Mac and Linux computers as well as iPhone and Android phones. For it to work in a device it had to be installed and you had to signup with a single account. With the credentials of the account you can go to their website and access the devices history of positioning.

go to [Prey's login site](https://panel.preyproject.com/login?redirect=/app) 

Continue

### contact_an_localize

Access Now’s Digital Security Helpline works with individuals and organizations around the world to keep them safe online. If you’re at risk, we can help you improve your digital security practices to keep out of harm’s way. If you’re already under attack, we provide rapid-response emergency assistance.

If you would like to block the missing person's accounts go to [AccessNow's helpline](https://www.accessnow.org/help/)

Do you 

### legal_support

Would you need legal support such as expertise or a lawyer ?

  - [Yes](#contact_legal)
  - [No](#)



### psycological

Would you need individual care such as information how to deal with stress or a psychologist?


  - [Yes](#contact_individual_care)
  - [No](#)



Is this something that might help your friend?
Yes --> do the things below
no -->
I don't know-->


Have you reached them yourself? 
    ##detained? y/n 
    
Yes: [here you are able to do a little assessment together with them about what to do next]
    Are their devices safe?
    ##devices confiscated y/n
    
    

No:
    tried alternative means of reaching them?
    Yes: go above
    No: 
        Have you tried other means of reaching them by other ways?
        This includes police station, embassy, media....
        Yes: go above
        No: .......

###Arrested_YES:
<<< Will you be at risk by trying to locate/help them?>>>>
- web browsing security
- communication security
- physical 
- self care

What is their current status?
Arrested_Yes/No
DeviceConfiscated_YES/NO

Do you know where their devices are?

Do you have their credentials?

Y: you change below
N: ask <Access Now> for help:

Secure (social media) accounts
change passwords
disable accounts


Examples of social media platforms include: facebook
twitter
instagream
google 
yahoo

Contact family
contact work
contact lawyer
contact embassy (if out of country)



Services to link from here:
    legal services
    public advocacy
    individual care       
-Grants & Funding
-In-Person Training
-Organizational Security 
-Website Hosting
-Website Protection (Denial of Service Protection)
-24/7 digital support (intake)
-Relocation
-Equipment replacement
-Assessing Threats and Risks
-Securing Communications
-Device Security
-Vulnerabilities and Malware
-Web Browsing Security
-Securing Social Media Accounts
-Online Harassment Mitigation (Doxxing, Trolling)
-Forensic Analysis
-Legal Support
-individual care
[Other]
    

















Resources